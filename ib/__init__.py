#!/usr/bin/env python
# -*- coding: utf-8 -*-

##
# IbPy package root.
#
##
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

# these values substituted during release build.
from .version import __version__, __ibpyversion__



