#!/usr/bin/env python
# -*- coding: utf-8; py-indent-offset:4 -*-
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)


__version__ = '1.0.3'

__ibpyversion__ = tuple(int(x) for x in __version__.split('.'))